import binance
import os 
import copy 
import time 
import sys
import random
import pandas as pd
import numpy as np
import time
import pickle
import warnings
import logging
import json
import pandas_ta
import matplotlib.pyplot as plt
import matplotlib.dates as mpl_dates
import gym
import datetime   

from collections import deque

from ta.trend import (
    SMAIndicator,
    macd,
    PSARIndicator
)
from ta.volatility import BollingerBands
from ta.momentum import rsi
from binance.enums import *
from binance.exceptions import BinanceAPIException, BinanceOrderException
from binance import (
    Client,
    ThreadedWebsocketManager,
    ThreadedDepthCacheManager
)
from gym import spaces
from sklearn import preprocessing
from stable_baselines3 import PPO
from stable_baselines3.common.vec_env import DummyVecEnv, VecNormalize
from smtplib import SMTP
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from pretty_html_table import build_table
from typing import (
    Optional,
    List,
    Tuple,
    Dict,
    Union
)

from config import API_Key, Secret_Key
from abc import abstractmethod
from typing import Callable, Iterable, List
from google.cloud import storage


class Transforms: 
    
    def transform(
        self,
        iterable: Iterable,
        inplace: bool = True,
        columns: List[str] = None,
        transform_fn: Callable[[Iterable], Iterable] = None
    ):
        """ Adapted from https://github.com/notadamking/RLTrader/blob/master/lib/data/features/transform.py """
        if inplace is True:
            transformed_iterable = iterable
        else:
            transformed_iterable = iterable.copy()

        if isinstance(transformed_iterable, pd.DataFrame):
            is_list = False
        else:
            is_list = True
            transformed_iterable = pd.DataFrame(
                transformed_iterable,
                columns=columns
            )

        transformed_iterable.fillna(0, inplace=True)

        if transform_fn is None:
            raise NotImplementedError()

        if columns is None:
            columns = transformed_iterable.columns

        for column in columns:
            transformed_iterable[column] = transform_fn(transformed_iterable[column])

        transformed_iterable.fillna(
            method="bfill",
            inplace=True
        )
        transformed_iterable[np.bitwise_not(np.isfinite(transformed_iterable))] = 0

        if is_list:
            transformed_iterable = transformed_iterable.values

        return transformed_iterable

    def max_min_normalize(self, iterable: Iterable, inplace: bool = True, columns: List[str] = None):
        return transform(
            iterable,
            inplace,
            columns,
            lambda t_iterable: (t_iterable - t_iterable.min()) / (t_iterable.max() - t_iterable.min())
        )

    def mean_normalize(
        self, 
        iterable: Iterable,
        inplace: bool = True,
        columns: List[str] = None
    ):
        return transform(
            iterable,
            inplace,
            columns,
            lambda t_iterable: (t_iterable - t_iterable.mean()) / t_iterable.std()
        )

    def difference(
        self, 
        iterable: Iterable,
        inplace: bool = True,
        columns: List[str] = None
    ):
        return transform(
            iterable,
            inplace,
            columns,
            lambda t_iterable: t_iterable - t_iterable.shift(1)
        )

    def log_and_difference(
        self,
        iterable: Iterable,
        inplace: bool = True,
        columns: List[str] = None
    ):
        return transform(
            iterable,
            inplace,
            columns, 
            lambda t_iterable: np.log1p(t_iterable) - np.log1p(t_iterable).shift(1)
        )

    def identity(
        self,
        iterable: Iterable,
        inplace: bool = True,
        columns: List[str] = None
    ):
        return iterable

class Logger:  
    """Logger/Debugger of Binance Manager"""
    
    def __init__(
        self,
        handle:str = "Binance Live Trading Bot",
        debug:bool = False
    ):
        
        if debug:
            logging.basicConfig()
            logging.root.setLevel(logging.NOTSET)
            logging.basicConfig(level=logging.NOTSET)
            self.logger = logging.getLogger(handle)
            
        else:
            
            self.logger = logging.getLogger(handle)
            self.logger.info("CryptoRL Debug mode is off!")
            
        self.handle = handle
        
    def get_logger(self) -> logging.Logger:
        return self.logger
        
    def info(self) -> None:    
        print(f"Convenient methods in order of verbosity from highest to lowest")
        logger.debug("this will get printed")
        logger.info("this will get printed")
        logger.warning("this will get printed")
        logger.error("this will get printed")
        logger.critical("this will get printed")
        
    def _set_var(
        self,
        var
    ):
        """ Setting class variables by dict """
        for key, value in var.items():
            setattr(self, key, value)


class DiscreteGymEnvironment(gym.Env):

    def __init__(
        self,
        ohclv_size:int = 5,
        lookback_window_size:int = 300,
        num_extra_feature:int = 9
    ):
        """
        Actions of the format Buy 1/10, Sell 3/10, Hold, etc. Action space is represented as discrete set of 3 options
        i.e., buy, sell and hold. And, another discrete set of 10 amounts (1/10,...,10/10). When the buy is selected, we will buy
        amount * self.balance worth of crypto. For the sell action, we will sell amount * self.btc_held worth of BTC. 
        Hold action ignores the amount and do nothing Actions of the format Buy 1/10, Sell 3/10, Hold (amount ignored), etc.
        """

        super(DiscreteGymEnvironment, self).__init__()

        self.action_space = spaces.MultiDiscrete([3, 10])

        # Prices contains the OHCLV (Open,High,Close,Low,Volume) values, net worth and trade history
        self.observation_space = spaces.Box(
            low=0,
            high=1,
            shape = (lookback_window_size, ohclv_size + 5 + num_extra_feature),
            dtype=np.float16
        )
        
        
    def action_decompose(self, action):
        """ Returns decomposed action, action type and amount """
        action_type = action[0]
        amount = action[1] / 10
        return action_type, amount


class BinanceWalletManager(Logger):
    """ Live Binance Manager for transactions & wallet calculations """
    
    def __init__(
        self, 
        client:binance.Client,
        transaction_memory_len:int = 1000
    ):
        super(BinanceWalletManager, self).__init__()

        self.client = client
        self.log_time = datetime.datetime.now().time()
        self.transaction_memory_len = transaction_memory_len
        self.order_history = deque(maxlen = transaction_memory_len)
        
    def get_balances(self) -> pd.DataFrame:
        """ Returns the asset-amount pairs """
        balances = self.client.get_account()['balances']
        balances = pd.DataFrame(balances)

        return balances[balances['free'].astype(float) != 0.0]
    
    def get_asset_balance(
        self,
        asset:str = "BTC"
    ) -> float:
        """ Given the asset name, returns the amount of given asset"""
        return float(
            self.client.get_asset_balance(asset=asset)['free']
        )

    def get_usdt_balance(self):
        return float(
            client.get_asset_balance('USDT')['free']
        )
    
    def get_account(self) -> dict:
        """ Get balances for all assets & some account information"""
        self.account = self.client.get_account()
        return self.account
    
    def spot_balance(self) -> dict:
        """ Get spot balance in both BTC and USDT"""
        sum_btc = 0.0
        balances = self.client.get_account()
        for b in balances["balances"]:
            asset = b["asset"]
            
            if float(b["free"]) != 0.0 or float(b["locked"]) != 0.0:
                try:
                    btc_quantity = float(b["free"]) + float(b["locked"])
                    
                    if asset == "BTC":
                        sum_btc += btc_quantity
                        
                    elif asset == "USDT":
                        p = self.client.get_symbol_ticker(symbol = asset + "BTC")
                        sum_btc += btc_quantity * float(p["price"])
                    else:
                        pass
                except:
                    pass

        current_btc_price_USD = self.client.get_symbol_ticker(symbol="BTCUSDT")["price"]
        own_usd = sum_btc * float(current_btc_price_USD)
        
        return {
            'total_btc': sum_btc,
            'USDT': own_usd
        }
    
    def get_order_history(self) -> dict:
        """ Returns order history (Balance, Net Worth, Crypto Bought, Crypto Sold, Crypto Held)"""
        balance = self.get_usdt_balance()
        spot_balance = self.spot_balance()
        
        net_worth = spot_balance['USDT']
        crypto_held = spot_balance['total_btc']
        
        transaction_memory = self.transaction_history[-1]
        signal = transaction_memory['signal']
        rounded_Qty = transaction_memory['rounded_Qty']
        
        crypto_bought = rounded_Qty if signal == 'BUY' else 0.0
        crypto_sold = rounded_Qty if signal == 'SELL' else 0.0
        
        order_history = {
                'balance': balance,
                'net_worth' : net_worth,
                'crypto_bought': crypto_bought,
                'crypto_sold' : crypto_sold,
                'crypto_held' : crypto_held
        }
        
        
        return order_history


class BinanceTransaction(BinanceWalletManager):
    
    def __init__(
        self,
        client:binance.Client,
        first_transaction:bool = True
    ):
        super(BinanceTransaction, self).__init__(client)
        
        self.transaction_history = deque(maxlen = self.transaction_memory_len)
        
        if first_transaction:
            current_price = float(
                client.get_symbol_ticker(
                    symbol="BTCUSDT"
                )['price']
            )
            min_transaction_lim = 14
            
            self.buy_asset(
                Qty =  min_transaction_lim/current_price,
                print_transaction = False
            )
        
    def __str__(self):
        return str(self.transaction_history)
    
    def __repr__(self):
        return str(self.transaction_history)
        
    def get_transaction_history(self) -> deque:
        """ Returns last 1000 transactions"""
        return self.transaction_history

    def get_transaction_history_excel(self) -> pd.DataFrame:
        """ Returns last 1000 transaction in pd.DataFrame """
        
        transactions_df = pd.DataFrame(self.transaction_history)
        transactions_df['time'] = pd.to_datetime(transactions_df['time'])
        return transactions_df.set_index('time').sort_index(ascending=False)
        
    def _is_transaction_valid(
        self, 
        transaction_cost:float,
        threshold:float = 13,
        double_check:bool = False
    ) -> bool:
        """ Given the quantity of the crypto and symbol, checks if transaction is bigger than 10.3 USDT """

        higher_than_threshold = transaction_cost >= threshold
    
        return bool(
            higher_than_threshold and self._is_Qty_valid(Qty, symbol)
        ) if double_check else bool(
            higher_than_threshold
        )  

    def _is_Qty_valid(
        self,
        Qty:float,
        symbol:str = 'BTCUSDT'
    ) -> bool:
            """ Given the quantity of the crypto and symbol, checks if quantity is in valid range """

            info = client.get_symbol_info(symbol)

            maxQty = float(
                info['filters'][2]['maxQty']
            )
            minQty = float(
                info['filters'][2]['minQty']
            )

            return maxQty >= Qty >= minQty
        
    def _compute_precision(
        self,
        symbol:str = 'BTCUSDT'
    ) -> int:
            """ Given the ticker of crypto, returns the # of precision limit"""

            info = client.get_symbol_info(symbol)
            val = info['filters'][2]['stepSize']
            decimal = 0
            is_dec = False
            for c in val:
                if is_dec is True:
                    decimal += 1
                if c == '1':
                    break
                if c == '.':
                    is_dec = True
                    
            return decimal
    
    def _apply_base_precision(
        self,
        Qty:float,
        symbol:str = 'BTCUSDT',
        precision:int = 6
    ) -> float:
        """ Given the quantity of the crypto and symbol, rounds to a defined precision in binance limits """

        precision = precision if precision is not None else self._compute_precision(symbol)
        Qty = float(Qty) if not isinstance(Qty, float) else Qty 
        return round(Qty, precision)
    
    def _round_transaction(
        self,
        Qty:float,
        symbol:str = 'BTCUSDT',
        upper_threshold:float = 14,
        lower_threshold:float = 5.0
    ) -> float:
        """ Given the quantity of the crypto and symbol, checks if transaction is bigger than upper threshold USDT """

        Qty = float(Qty) if not isinstance(Qty, float) else Qty   

        symbol_price = float(
            client.get_symbol_ticker(symbol=symbol)['price']
        )

        transaction_cost = Qty * symbol_price

        if self._is_transaction_valid(transaction_cost, upper_threshold):

            rounded_transaction = transaction_cost
            new_Qty = Qty

        elif upper_threshold > transaction_cost >= lower_threshold:
            rounded_transaction = upper_threshold 
            new_Qty = rounded_transaction / symbol_price

        elif lower_threshold > transaction_cost >= 0.0:
            rounded_transaction = 0.0
            new_Qty = 0.0
            
        else:
            assert False, f"Invalid Transaction cost & asset quantity {(transaction_cost, Qty)}"

        return self._apply_base_precision(new_Qty), symbol_price

    def _create_test_order(
        self, 
        Qty:float,
        side:str,
        symbol:str = "BTCUSDT"
    ) -> dict:
        """ Given the quantity and symbol ticker, buys the asset in Binance """
    
        success = False
        
        try:
            buy_limit = client.create_test_order(
                symbol = symbol,
                side = side,
                type = 'MARKET',
                quantity = Qty,
        )
            
            success = True
            
        except BinanceAPIException as e:
            logging.warning(e)
            
        except BinanceOrderException as o:
            logging.warning(o)
            
        return success
    
    def _get_asset_balance(
        self,
        asset:str = "BTC"
    ) -> float:
        """ Given the asset name, returns the amount of given asset"""
        return float(
            client.get_asset_balance(asset=asset)['free']
        )
    
    def buy_asset(
        self,
        Qty:float,
        symbol:str = "BTCUSDT",
        print_transaction:bool = True
    ) -> dict:
        """ Given the quantity and symbol ticker, buys the asset in Binance """
        signal = 'HOLD'
        
        rounded_Qty, symbol_price = self._round_transaction(Qty, symbol)
        
        buy_limit = {}
        
        if rounded_Qty != 0:
            
            balance = self._get_asset_balance('USDT')
            transaction_cost = rounded_Qty * symbol_price
            
            if balance > transaction_cost:
                
                success_test_order = self._create_test_order(
                    rounded_Qty,
                    side = 'BUY',
                    symbol = symbol
                )

                if success_test_order:

                    try:
                        buy_limit = client.create_order(
                            symbol= symbol,
                            side ='BUY',
                            type ='MARKET',
                            quantity = rounded_Qty,
                        )

                        signal = 'BUY'

                    except BinanceAPIException as e:
                        self.logger.warning(e)

                    except BinanceOrderException as o:
                        self.logger.warning(o)
            

                else:
                    self.logger.warning(f"Test buy order fail!, real order is not created.")
            else:
                self.logger.info(f" There is not enough balance to buy. Total balance: {balance} and transaction cost: {transaction_cost}")
                
        else:
            self.logger.warning(f"Transaction is lower than threshold!, Qty : {Qty}, order is not created")
    
            
        model_cache = {
               'time': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
               'rounded_Qty': rounded_Qty,
               'Qty': Qty, 
               'signal': signal
        }
        
        
        balance = self.get_usdt_balance()
        spot_balance = self.spot_balance()
        crypto_held = self._get_asset_balance('BTC')
        
        net_worth = balance + crypto_held * symbol_price
        
        crypto_bought = rounded_Qty if signal == 'BUY' else 0.0
        crypto_sold = rounded_Qty if signal == 'SELL' else 0.0
        
        order_cache = {
                'balance': balance,
                'net_worth' : net_worth,
                'crypto_bought': crypto_bought,
                'crypto_sold' : crypto_sold,
                'crypto_held' : crypto_held
        }
        
        price_cache = {
               'symbol_price' : symbol_price
        }
        
        cache = {
            **buy_limit,
            **model_cache,
            **order_cache,
            **price_cache
        } 
        
        self._set_var(cache)
        
        if print_transaction: print(json.dumps(cache, indent=4, sort_keys=True))
            
        self.transaction_history.append(cache)
        
        return cache
  
    def sell_asset(
        self,
        Qty:float,
        symbol:str = "BTCUSDT",
        print_transaction:bool = True
    ) -> dict:
        """ Given the quantity and symbol ticker, sells the asset in Binance """
        
        signal = 'HOLD'
        
        rounded_Qty, symbol_price = self._round_transaction(Qty, symbol)
        sell_limit = {}
        
        if rounded_Qty != 0:
            
            
            asset_held = self._get_asset_balance('BTC')
            
            if asset_held > rounded_Qty:
            
                success_test_order = self._create_test_order(
                        rounded_Qty,
                        side = 'SELL',
                        symbol = symbol
                )

                if success_test_order:

                    try:
                        sell_limit = client.create_order(
                            symbol = symbol,
                            side ='SELL',
                            type ='MARKET',
                            quantity = rounded_Qty,
                        )

                        signal = 'SELL'

                    except BinanceAPIException as e:
                        self.logger.warning(e)

                    except BinanceOrderException as o:
                        self.logger.warning(o)

                else:
                    self.logger.warning(f"Test sell order fail!, real order is not created.")
                    
                    
            else:
                self.logger.info(f" There is not enough asset to sell. Total asset: {asset_held} and quantity desired to be sold: {rounded_Qty}")
                
                    
        else:
            self.logger.warning(f"Transaction is lower than threshold!, Qty : {Qty}, order is not created")
    
            
        model_cache = {
               'time': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
               'rounded_Qty': rounded_Qty,
               'Qty': Qty, 
               'signal': signal
        }
        
        
        balance = self.get_usdt_balance()
        spot_balance = self.spot_balance()
        crypto_held = self._get_asset_balance('BTC') 
        
        net_worth = balance + (crypto_held * symbol_price)
        
        crypto_bought = rounded_Qty if signal == 'BUY' else 0.0
        crypto_sold = rounded_Qty if signal == 'SELL' else 0.0
        
        order_cache = {
                'balance': balance,
                'net_worth' : net_worth,
                'crypto_bought': crypto_bought,
                'crypto_sold' : crypto_sold,
                'crypto_held' : crypto_held
        }
        
        price_cache = {
               'symbol_price' : symbol_price
        }
       
        cache = {
            **sell_limit,
            **model_cache,
            **order_cache,
            **price_cache
        }
        
        self._set_var(cache)
        
        if print_transaction: print(json.dumps(cache, indent=4, sort_keys=True))
            
        self.transaction_history.append(cache)
        
        return cache
    
    def process_transaction(
        self,
        transaction:dict,
        symbol:str = "BTCUSDT",
        print_transaction:bool = True
    ) -> dict:
        
        """ Processing & mapping transaction data to real transaction"""
        signal = transaction['signal']
        Qty = transaction['Qty']
        
        cache = {}
        
        if signal in ['SELL', 'Sell', 'sell']:
            cache = self.sell_asset(
                Qty,
                symbol,
                print_transaction
            )
            
        elif signal in ['BUY', 'Buy', 'buy']:
            cache = self.buy_asset(
                Qty,
                symbol,
                print_transaction
            )
            
        else:
            self.logger.critical(f"Transaction could not resolved: {signal}, expected BUY or SELL")
            
        return cache
    
class BinanceManager(BinanceTransaction):
    def __init__(
        self, 
        client
    ):
        super(BinanceManager, self).__init__(client)

class LiveBinanceDataManager:  
    
    def __init__(self):
        self.data = None
    
    def _add_indicator(
        self,
        df:pd.DataFrame, 
        indicator_list:list = [
            'sma7','sma25','sma99','bb_bbm','bb_bbh',
            'bb_bbl','psar','MACD','RSI','ema7','ema25',
            'ema99', 'super_trend', 'super_trend_s',
            'super_trend_l', 'log_return'
        ]
    ) -> pd.DataFrame:

        """ Adding list of indicators and fill nan-values """

        assert all(indicator in [
            'sma7','sma25','sma99','bb_bbm','bb_bbh',
            'bb_bbl','psar','MACD','RSI','ema7','ema25',
            'ema99', 'super_trend', 'super_trend_s',
            'super_trend_l', 'log_return'
        ] 

               for indicator in indicator_list), f'Unknown indicator {indicator}!'

        if 'sma7' in indicator_list:
            df["sma7"] = SMAIndicator(
                close=df["Close"],
                window=7,
                fillna=True
            ).sma_indicator()

        if 'sma25' in indicator_list:
            df["sma25"] = SMAIndicator(
                close=df["Close"],
                window=25,
                fillna=True
            ).sma_indicator()

        if 'sma99' in indicator_list:
            df["sma99"] = SMAIndicator(
                close=df["Close"],
                window=99,
                fillna=True
            ).sma_indicator()

        if 'ema7' in indicator_list:   
            df["ema7"] = pandas_ta.ema(
                df["Close"],
                length=7
            ).fillna(method = 'backfill')

        if 'ema25' in indicator_list:   
            df["ema25"] = pandas_ta.ema(
                df["Close"],
                length=25
            ).fillna(method = 'backfill')

        if 'ema99' in indicator_list:   
            df["ema99"] = pandas_ta.ema(
                df["Close"],
                length=99
            ).fillna(method = 'backfill')

        indicator_bb = BollingerBands(
            close=df["Close"],
            window=20,
            window_dev=2
        )

        if 'bb_bbm' in indicator_list:
            df['bb_bbm'] = indicator_bb.bollinger_mavg().fillna(method = 'backfill')

        if 'bb_bbh' in indicator_list:     
            df['bb_bbh'] = indicator_bb.bollinger_hband().fillna(method = 'backfill')

        if 'bb_bbl' in indicator_list: 
            df['bb_bbl'] = indicator_bb.bollinger_lband().fillna(method = 'backfill')

        if 'psar' in indicator_list: 
            indicator_psar = PSARIndicator(
                high=df["High"],
                low=df["Low"],
                close=df["Close"],
                step=0.02,
                max_step=2,
                fillna=True
            )
            df['psar'] = indicator_psar.psar()

        if 'MACD' in indicator_list: 
            df["MACD"] = macd(
                close=df["Close"],
                window_slow=26,
                window_fast=12,
                fillna=True
            ) 

        if 'RSI' in indicator_list: 
            df["RSI"] = rsi(
                close=df["Close"],
                window=14, 
                fillna=True
            )

        if 'super_trend' in indicator_list: 
            super_trend = pandas_ta.supertrend(
                high=df["High"],
                low = df["Low"],
                close = df["Close"],
                length = 10,
                multiplier=4.0
            )
            df['super_trend'] = super_trend['SUPERT_10_4.0'].fillna(method = 'backfill')

            if 'super_trend_s' in indicator_list: 
                df['super_trend_s'] = super_trend['SUPERTl_10_4.0'].fillna(method = 'backfill').fillna(method = 'ffill')

            if 'super_trend_l' in indicator_list: 
                df['super_trend_l'] = super_trend['SUPERT_10_4.0'].fillna(method = 'backfill')

        if 'log_return' in indicator_list: 
            df['log_return'] = pandas_ta.log_return(
                df['Close'],
                cumulative=False
            ).fillna(method = 'backfill')

        if 'percent_return' in indicator_list: 
            df['percent_return'] = pandas_ta.percent_return(
                df['Close'],
                cumulative=False
            ).fillna(method = 'backfill')   

        return df
    
    def _preprocess(
        self, 
        df:pd.DataFrame, 
        indicator_list:list = [
           'sma7','sma25','sma99','bb_bbm','bb_bbh',
           'bb_bbl','psar','MACD','RSI','ema7','ema25',
           'ema99', 'super_trend', 'super_trend_s',
           'super_trend_l', 'log_return'
        ]
                   
    ) -> pd.DataFrame:
        """ Given the data frame consist of OHLCV with date times, reads and adds indicator
            with given list. Save preprocessed data frame if save_preprocessed True """

        df = df[[
            'Date','Open','High','Low','Close', 'Volume'
        ]].sort_values('Date')

        df['Date'] = df['Date'].apply(lambda x : str(x)[:16])

        assert self._unique_cols(df['Date'].apply(lambda x : len(x))), 'Date length must be same!'

        if len(indicator_list) > 0:
            df = self._add_indicator(
                df,
                indicator_list
            )

        if 'Unnamed: 0' in df.columns.tolist():
            df.drop(['Unnamed: 0'], 1, inplace = True)

        return df
    
    def _unique_cols(
        self,
        df:pd.DataFrame
    ) -> pd.DataFrame:
        a = df.to_numpy()
        return (a[0] == a).all(0)
    

    def _fetch_binance_data(
        self,
        coin_list:list = ["BTCUSDT"],
        time_interval:str = Client.KLINE_INTERVAL_15MINUTE, 
        last_time_checkpoint = "1 Jan, 2016"
    ):

        """ Fetching and saving OCHLV from Binance API with given coin names and time interval. 

            Args:
                - coin_list (list):     List of Coin Symbols (Default BTCUSDT)
                - time_interval (str):  Time Interval (Default 15m)

            Returns:
                - cryto_data (dict):  Dict of data frames, where keys are crypto names and values are pd.DataFrame's.
        """

        if not isinstance(coin_list, list):
            coin_list = [coin_list]

        crypto_data = {}


        for crypto_symbol in coin_list:

            print(f"{crypto_symbol} is fetching from Binance API.")

            klines = client.get_historical_klines(
                crypto_symbol,
                time_interval,
                last_time_checkpoint
            )
            df_klines = pd.DataFrame(
                klines, 
                columns = [
                     'Date', 'Open', 'High', 'Low', 'Close',
                     'Volume', 'closeTime', 'quoteAssetVolume',
                     'NumTrade', 'takerBuyBaseVol', 'takerBuyQuoteVol',
                     'ignore'
                 ]
            )

            df_klines['Date'] = pd.to_datetime(
                df_klines['Date'],
                unit='ms'
            )
            df_klines = df_klines[[
                'Date', 'Open', 'High', 'Low', 'Close', 'Volume'
            ]]


            crypto_data[crypto_symbol] = df_klines

            return crypto_data
    
    def _to_numeric(
        self, 
        df:pd.DataFrame,
        numeric_cols = [
            'Open', 'High', 'Low', 'Close', 'Volume'
        ]
    ) -> pd.DataFrame:  
        """ Converting non-numeric columns to numeric columns. """
        
        for numeric_col in numeric_cols:
            df[numeric_col] = df[numeric_col].astype(float)
             
        return df
        
    def get_preprocessed_data(
        self,
        coin_list:list = ["BTCUSDT"],
        time_interval:str = Client.KLINE_INTERVAL_15MINUTE, 
        last_time_checkpoint = "1 Jan, 2016",
        indicator_list:list = [
           'sma7','sma25','sma99','bb_bbm','bb_bbh',
           'bb_bbl','psar','MACD','RSI','ema7','ema25',
           'ema99', 'super_trend', 'super_trend_s',
           'super_trend_l', 'log_return'
        ]
                              
    ) -> pd.DataFrame:
        
        online_data_dict = self._fetch_binance_data(
            coin_list,
            time_interval,
            last_time_checkpoint
        )
        
        df = online_data_dict[coin_list[0]]
        df_numeric = self._to_numeric(df) 
        
        self.data = self._preprocess(
            df_numeric,
            indicator_list
        )
        
        return self.data

    def min_max(
        self,
        df:pd.DataFrame
    ) -> pd.DataFrame:
        return (df - df.min(0))/ (df.max(0) - df.min(0))
    
    def mean_norm(
        self,
        df:pd.DataFrame
    ) -> pd.DataFrame:
        return (df - df.mean(0)) / df.std(0)

    def get_lockback_time(
        self
    ) -> datetime.datetime:
        return (
            datetime.datetime.now() -  datetime.timedelta(days = 17)
        ).strftime(
            '%Y-%m-%d %H:%M:%S'
        )

class ActionHandler:
    actions = {
        0: 'HOLD',
        1: 'BUY',
        2: 'SELL'
    }
    
    def __init__(self):
        self.positions = deque(maxlen = 1500)
  
    def action_decompose(
        self,
        action
    ):
        """ Returns decomposed action, action type and amount """
        action_type = action[0]
        amount = action[1] / 10
        return action_type, amount
    
    def step(
        self,
        action,
        balance,
        current_price,
        crypto_held
    ):
        
        action_type, amount = self.action_decompose(action)
        
        assert action_type in [0, 1, 2], f'Unknown action type found : {action_type}, should be in [0, 1, 2]'
        assert 0.0 <= amount <= 1.0, f'Unknown amount type found : {amount}, should be in [0, 0.1, 0.2, ... , 1]'
        
        signal = self.actions[action_type]
        Qty = 0
        
        if signal == 'HOLD':
            pass
            
        elif signal == 'BUY':
            Qty = balance / current_price * amount

        elif signal == 'SELL':
            Qty = crypto_held * amount
            
        else:
            print(f"Unallowed signal: {signal}, signal is changed to HOLD")
            signal = 'HOLD'
              
        cache = {
            'signal': signal,
            'Qty': Qty
        }
        
        self.positions.append(cache)
        
        return cache

class LiveUtils:
    
    @staticmethod
    def get_latest_order_info(binance_manager:BinanceManager) -> tuple:
        # Latests order information:
        balance = binance_manager.balance
        net_worth = binance_manager.net_worth
        crypto_bought = binance_manager.crypto_bought
        crypto_sold = binance_manager.crypto_sold
        crypto_held = binance_manager.crypto_held

        return balance, net_worth, crypto_bought, crypto_sold, crypto_held
    
    @staticmethod
    def update_order_dict(
        order_dict:dict,
        balance,
        net_worth,
        crypto_bought,
        crypto_sold,
        crypto_held
    ) -> dict:
        """ Deque appending order information """
        
        order_dict['balance'].append(balance)
        order_dict['net_worth'].append(net_worth)
        order_dict['crypto_bought'].append(crypto_bought)
        order_dict['crypto_sold'].append(crypto_sold)
        order_dict['crypto_held'].append(crypto_held)

        return order_dict
    
    @staticmethod
    def get_pretrained_ppo_model(
        model_path:str = "pretrained/Good/PPO_discrete_lr0.001__step50000_2021-06-18-15:26",
        env:DiscreteGymEnvironment = DiscreteGymEnvironment(),
    ) -> PPO:
    
        return PPO('MlpPolicy', env).load(os.path.join(model_path, 'models', 'model'))
    
    @staticmethod
    def load_pretrained_model(
        model_path:str = "models/PPO_discrete_lr0.001__step50000_2021-06-18-15:26",
        google_storage:bool = True,
        env:DiscreteGymEnvironment = DiscreteGymEnvironment()
    ):
        return PPO('MlpPolicy', env).load(model_path)
        
    
    @staticmethod
    def get_lockback_time() -> datetime.datetime:
        return (datetime.datetime.now() -  datetime.timedelta(days = 30)).strftime('%Y-%m-%d %H:%M:%S')
    
    @staticmethod
    def get_order_dict(
        df:pd.DataFrame,
        order_columns:list,
        window_size:int
    ) -> dict:
        
        order_dict = {}
        for order_col in order_columns:
            window_deque = deque(maxlen = window_size)
            for val in df[order_col]:
                window_deque.append(val)
            order_dict[order_col] = window_deque

        return order_dict

    @staticmethod
    def split_as_data_types(
        df,
        ohlcv_list,
        indicator_list
    ):
        """ Split market, order and indicator data """
        df_market = df.loc[:, ohlcv_list]
        df_indicator = df.loc[:, indicator_list]

        return df_market, df_indicator
    
    @staticmethod
    def is_candle_open():
        return True
    
    @staticmethod
    def send_mail(
        body,
        subject = 'CryptoRL Made New Transaction in Binance!',
        recipients = [
            'can.kocagil@oredata.com',
            'omer.kurt@oredata.com'
        ]
    ):
        message = MIMEMultipart()
        message['Subject'] = subject
        message['From'] = 'cankocagil123@gmail.com'
        message['To'] = 'cankocagil123@gmail.com'

        emaillist = [elem.strip().split(',') for elem in recipients]

        body_content = body
        message.attach(MIMEText(body_content, "html"))
        msg_body = message.as_string()

        server = SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(message['From'], password='monaqcwldouzeouq')
        server.sendmail(message['From'], emaillist, msg_body)
        server.quit()
      
    @staticmethod
    def df2html(
        df_transactions:pd.DataFrame,
        color = 'green_light'
    ) -> str:
        return build_table(
            df_transactions,
            color = color
    )
    
    @staticmethod
    def send_transactions(
        transaction_history:dict,
        transactions_cols:list
    ) -> None:
        df_transactions = pd.DataFrame(transaction_history).sort_values('time', ascending=False)
        df_transactions = df_transactions[transactions_cols]
        html_transactions = live_utils.df2html(df_transactions)
        live_utils.send_mail(html_transactions)
    
    @staticmethod
    def scale_state(
        scaler:preprocessing.MinMaxScaler,
        state:np.ndarray
    ) -> np.ndarray:
        return scaler.fit_transform(state)
    
    @staticmethod
    def save_obj(
        obj:object,
        path:str = None,
        google_storage:bool = True
    ) -> None:
        with open(path + '.pkl', 'wb') as f:
            pickle.dump(
                obj,
                f, 
                pickle.HIGHEST_PROTOCOL
            )

    @staticmethod
    def load_obj(
        path:str = None,
        google_storage:bool = True
    ) -> object:
        with open(path + '.pkl', 'rb') as f:
            return pickle.load(f)

    @staticmethod
    def save(
        data:np.ndarray = None,
        path:str = None
    ) -> None:
        np.save(path + '.npy', data, allow_pickle=True)

    @staticmethod
    def load(
        path:str = None
    ) -> np.ndarray:
        return np.load(path + '.npy', allow_pickle=True)  

    @staticmethod
    def insert_scalar_to_frame(
        join_df:pd.DataFrame,
        col_name:str,
        scalar: Union[int, float],
        window_size: Union[int, float] = 1500
    ) -> pd.DataFrame:

        df = join_df.copy()
        df[col_name] = np.repeat(scalar, repeats=window_size, axis=0)

        return df  
    
def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket. The ID of your GCS bucket
        Args:
            - bucket_name = "your-bucket-name" & The path to your file to upload
            - source_file_name = "local/path/to/file"
            - destination_blob_name = "storage-object-name"
    """

    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print(
        "File {} uploaded to {}.".format(
            source_file_name, destination_blob_name
        )
    )

def download_blob(bucket_name, source_blob_name, destination_file_name):
    """Downloads a blob from the bucket."""
    # The ID of your GCS bucket
    # bucket_name = "your-bucket-name"

    # The ID of your GCS object
    # source_blob_name = "storage-object-name"

    # The path to which the file should be downloaded
    # destination_file_name = "local/path/to/file"

    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)

    # Construct a client side representation of a blob.
    # Note `Bucket.blob` differs from `Bucket.get_blob` as it doesn't retrieve
    # any content from Google Cloud Storage. As we don't need additional data,
    # using `Bucket.blob` is preferred here.
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)

    print(
        "Downloaded storage object {} from bucket {} to local file {}.".format(
            source_blob_name, bucket_name, destination_file_name
        )
    )
        

def apply_transact():


    folders = [
        '/tmp/',
    ]

    for folder in folders:
        if not os.path.exists(folder):
            os.makedirs(folder)
        
    model_dir = 'models'
    model_path = 'PPO_discrete_lr0.001__step50000_2021-06-18-15:26.zip'

    download_blob(
        bucket_name = "crypto-rl",
        source_blob_name = f"{model_dir}/{model_path}",
        destination_file_name = f"/tmp/{model_path}"
    )

    model = live_utils.load_pretrained_model(
        model_path = f"/tmp/{model_path}",
        google_storage = google_storage
    )

    state_dir = 'states'
    binance_manager_state_path = 'binance_manager'
    order_dict_state_path = 'order_dict'

    download_blob(
        bucket_name = "crypto-rl",
        source_blob_name = f"{state_dir}/{binance_manager_state_path}.pkl",
        destination_file_name = f"/tmp/{binance_manager_state_path}.pkl"
    )

    binance_manager = live_utils.load_obj(
        f"/tmp/{binance_manager_state_path}",
        google_storage = google_storage
    )

    download_blob(
        bucket_name = "crypto-rl",
        source_blob_name = f"{state_dir}/{order_dict_state_path}.pkl",
        destination_file_name = f"/tmp/{order_dict_state_path}.pkl"
    )

    order_dict = live_utils.load_obj(
        f"/tmp/{order_dict_state_path}",
        google_storage = google_storage
    )
        
    # Get last time checkpoint
    last_time_checkpoint = live_utils.get_lockback_time()

    # Fetch live market & indicator data:
    df = binance_data_manager.get_preprocessed_data(
        coin_list = coin_list,
        time_interval = time_interval,
        last_time_checkpoint = last_time_checkpoint,
        indicator_list = indicator_list
    )

    # Slicing last window_size & latest part of the data:
    df = df.iloc[-window_size:]

    # Latests order information:
    balance, net_worth, crypto_bought, crypto_sold, crypto_held = live_utils.get_latest_order_info(binance_manager)

    # Deque appending order information:
    order_dict = live_utils.update_order_dict(
        order_dict,
        balance,
        net_worth,
        crypto_bought,
        crypto_sold,
        crypto_held
    )

    # Arrange order history:
    df_order = pd.DataFrame(order_dict)

    # Split market, order and indicator data:
    df_market, df_indicator = live_utils.split_as_data_types(
        df,
        ohlcv_list,
        indicator_list
    )

    # Concatenate market, order and indicator data:
    state = np.concatenate([df_market, df_order, df_indicator], axis = 1)
    state = live_utils.scale_state(
        scaler, 
        state
    ) 
    state = state[-window_size:]

    assert np.isnan(state).sum() == 0, f"State has some NaN values"
    assert state.shape == (1500, 20), f"State shape should be (1500, 20). Get: {state.shape}"

    # Predicted action:
    action, _ = model.predict(
        state,
        deterministic=True
    )

    # Fetch balance, current symbol price and holdings:
    balance, current_price, crypto_held  = binance_manager.balance, binance_manager.symbol_price, binance_manager.crypto_held

    # Decompose action & create candidate transaction:
    transaction = action_handler.step(
        action,
        balance,
        current_price, 
        crypto_held
    )

    # Process real time transaction:
    transaction_cache = binance_manager.process_transaction(
        transaction,
        symbol,
        print_transaction
    )

    # Send transaction history via email:
    live_utils.send_transactions(
        binance_manager.transaction_history,
        transactions_cols
    )

    # Savings:
    live_utils.save_obj(
        binance_manager,
        f"/tmp/{binance_manager_state_path}",
        google_storage = google_storage
    )

    upload_blob(
        bucket_name = "crypto-rl",
        source_file_name = f"/tmp/{binance_manager_state_path}.pkl",
        destination_blob_name = f"{state_dir}/{binance_manager_state_path}.pkl"
    )

    live_utils.save_obj(
        order_dict,
        f"/tmp/{order_dict_state_path}",
        google_storage = google_storage
    )

    upload_blob(
        bucket_name = "crypto-rl",
        source_file_name = f"/tmp/{order_dict_state_path}.pkl",
        destination_blob_name = f"{state_dir}/{order_dict_state_path}.pkl"
    )

    return f"Succesful Prediction!"


if __name__ == "__main__":
    client = Client(
        api_key = API_Key,
        api_secret = Secret_Key
    )
    
    ohlcv_list = [
        'Open',
        'High',
        'Low',
        'Close',
        'Volume'
    ]

    indicator_list = [
        'psar',
        'MACD',
        'RSI',
        'ema7',
        'ema25',
        'ema99',
        'log_return',
        'super_trend',
        'super_trend_s',
        'super_trend_l'
    ]

    transactions_cols = [
        'time',
        'symbol_price',
        'signal',
        'balance', 
        'net_worth',
        'rounded_Qty',
        'Qty',
        'crypto_bought',
        'crypto_sold',
        'crypto_held'
    ]

    order_columns = [
        'balance',
        'net_worth',
        'crypto_bought',
        'crypto_sold',
        'crypto_held'
    ]

    coin_list = [
        "BTCUSDT"
    ]

    window_size = 1500
    symbol = coin_list[0]
    print_transaction = False
    time_interval = '15m'
    last_time_checkpoint = "30 day ago UTC" 
    google_storage = True

    live_utils = LiveUtils()
    binance_data_manager = LiveBinanceDataManager()
    action_handler = ActionHandler()
    scaler = preprocessing.MinMaxScaler()

    apply_transact()
