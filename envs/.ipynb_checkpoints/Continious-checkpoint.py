import os, copy, time, sys, random
import pandas as pd, numpy as np
from collections import deque
from datetime import datetime
import pickle
import warnings



# Normalization of observations:
from sklearn import preprocessing


import numpy as np

from utils.pipelines import preprocess_df, preprocess_price
from utils.split import time_split
# Custom Imports:
from render.graph import TradingGraph
from envs.to_gym import DiscreteGymEnvironment, ContiniousGymEnvironment
from utils.eval import eval_policy, evaluate
import preprocessing.transforms as T


import gym
from gym import spaces
from gym.utils import seeding


class MultiContiniousGymEnvironment(gym.Env):

    def __init__(self, ohclv_size:int = 5, lookback_window_size:int = 300, num_extra_feature:int = 9):

        super(MultiContiniousGymEnvironment, self).__init__()

        
        self.action_space = self.action_space = spaces.Box(
      low=np.array([0, 0]), high=np.array([3, 1]), dtype=np.float16)


        self.observation_space = spaces.Box(
            low = - 3, high = 3, shape = (lookback_window_size, ohclv_size + 5 + num_extra_feature), dtype=np.float16)
        
        
    def action_decompose(self, action):
        """ Returns decomposed action, action type and amount """
        action_type = action[0]
        action_type = self.discretize(action_type)
        amount = action[1]
        
        return action_type, amount
    
    def discretize(self, action):
        assert 0.0 <= action <= 3.0, f'Invalid action value encountered {action}, should be in [0,3]'
        
        action_type = math.floor(action)
            
        return action_type
    

class MultiContiniousStockEnvironment(MultiContiniousGymEnvironment):

    """A Quantitative Finance trading environment for OpenAI gym """
    metadata = {'render.modes': ['human', 'system', 'none']}
    standardizer = preprocessing.StandardScaler()
    scaler = preprocessing.MinMaxScaler()
    
    actions = {0: 'Hold',
               1: 'Buy',
               2: 'Sell'}
    
    viewer = None

    def __init__(self, df:pd.DataFrame, lookback_window_size:int = 50, initial_balance:float = 10000,
                 indicator_list:list = ['sma7', 'sma25','sma99','bb_bbm','bb_bbh', 'bb_bbl', 'psar', 'MACD', 'RSI'],
                 ohlcv_list:list = ['Open','High','Low','Close', 'Volume'],
                 commission:float = 0.00075, reward_strategy:str = 'PnL', sequential:bool=False, adjust_price:bool = True,
                 render_range:int = 100, show_reward:bool=False, show_indicators:bool=False, debug:bool = False,
                 slippage:float = 0.01, transform_obs:str = 'None', visualize:bool = False, dropna:bool = True,
                 normalize_obs:bool=True, name:str = "Custom Trading Environment", seed:int = None):
        
        super(MultiContiniousStockEnvironment, self).__init__(ohclv_size = len(ohlcv_list),
                                                              lookback_window_size = lookback_window_size,
                                                              num_extra_feature = len(indicator_list)
        )
       
        
        self._indicator_cols = ['sma7','sma25','sma99','bb_bbm','bb_bbh',
                                'bb_bbl','psar','MACD','RSI','ema7','ema25',
                                'ema99', 'super_trend', 'super_trend_s',
                                'super_trend_l', 'log_return',
                                'ETH_Open','ETH_High', 'ETH_Low', 'ETH_Close', 'ETH_Volume']
        
        self._reward_strategies = ['base','incremental', 'benchmark', 'sharp_ratio', 'PnL']
        
        self._transform_names = ['None', 'minmax', 'mean_norm', 'diff', 'log_diff']
        
        self._ohlcv_cols = ['Open','High','Low','Close', 'Volume']
        
        
        assert reward_strategy in self._reward_strategies, f'Unknown reward stratedy {reward_strategy}'
        assert transform_obs in self._transform_names, f'Unknown transformation for observation space {transform_obs}'
        assert all(indicator in  self._indicator_cols for indicator in indicator_list), 'Unknown indicator!'
        assert all(value in  self._ohlcv_cols for value in ohlcv_list), 'Unknown indicator!'
        
        if not isinstance(df, (pd.DataFrame)):
            warnings.warn(f'df object must be a pd.DataFrame, got {type(df)}') 
            
        if seed is not None:
            self._seed_manuel(seed)
            self.seed(seed)
        
        # Name of the environment:
        self.name = name

        self.df = df.dropna().reset_index() if dropna else df.fillna(method = 'backfill').reset_index()
        self.df_total_steps = len(self.df) - 1
        
        self.initial_balance = initial_balance
        self.lookback_window_size = lookback_window_size
        self.reward_strategy = reward_strategy
        
        self.indicator_list = indicator_list
        self.ohlcv_list = ohlcv_list
        
        self._features = {'features': ohlcv_list + indicator_list}
        
        # İf true => retuns the dimensions of the parameters in dict:
        self.debug = debug  
        self.shapes = {'df': self.df.shape} if self.debug else None

        self.commission = commission
        self.slippage = slippage       
        self.sequential = sequential
        self.normalize_obs = normalize_obs
        self.transform_obs = transform_obs
        
        
        self.transforms = [T.identity, T.max_min_normalize, T.mean_normalize, T.difference, T.log_and_difference]
        # See transformations:
        self.transform_dict = {name:transform for name, transform in zip(self._transform_names, self.transforms)}

        self.visualize = visualize
        self.render_range = render_range 
        self.show_reward = show_reward 
        self.show_indicators = show_indicators
        
        self.trade_minutes_in_year = 365*24*60
        self.excess_daily_return_ratio = 0.05 
        
        self.adjust_price = adjust_price

        self._start_trade_session()    
        
    def _start_trade_session(self):
        """ Initalizes the order, market and indicator history with fixed sizes """
        self.orders_history = deque(maxlen=self.lookback_window_size)
        self.market_history = deque(maxlen=self.lookback_window_size)
        self.indicators_history = deque(maxlen=self.lookback_window_size)
        
    def adjust_slippage(self, threshold:float = np.random.uniform(0.9, 1)):
        """ Slippage Modifier """
        chance = np.random.uniform(0, 1)

        if chance > threshold:
            self.slippage = np.random.uniform(0.0001, 0.005)
        else:
            self.slippage = np.random.uniform(0.00001, 0.0001)

    def get_order_history(self):
        """  Returns portolio of current state """
        return [self.balance, 
                self.net_worth,
                self.crypto_bought,
                self.crypto_sold,
                self.crypto_held]
    
    def apply_transform_obs(self, state):
        """  Applies the transformation to the observations """
        return self.transform_dict[self.transform_obs](state)
    
    def get_market_history(self, current_step):
        """ Returns  OHLCV """
        market_history = [self.df.loc[current_step, col] for col in self.ohlcv_list]
        return  market_history 
    
    def get_open_close(self):
        """ Returns Open and Close values in dict"""
        return {'Open' : self.df.loc[self.current_step, 'Open'],
                'Close': self.df.loc[self.current_step, 'Close']}
    
    def get_indicator_history(self, current_step):
        """ Returns list of indicators in the current step """
        indicators = [self.df.loc[current_step, indicator] for indicator in self.indicator_list]
        return indicators
    
    def _reset_session(self):
        """ Starts a new session """
        self.balance = self.initial_balance
        self.net_worth = self.initial_balance
        self.max_net_worth = self.initial_balance
        self.prev_net_worth = self.initial_balance
        self.crypto_held = 0
        self.crypto_sold = 0
        self.crypto_bought = 0
        self.episode_orders = 0 
        self.prev_episode_orders = 0
        self.punish_value = 0
        self.rewards = deque(maxlen=self.render_range)
        self.trades = deque(maxlen=self.render_range)
        
    def reset(self, n_iter = 500):
        """ Resets the environment and returns new observation """
        
        if self.sequential and self.visualize:
            self.visualization = TradingGraph(render_range = self.render_range, show_reward = self.show_reward, show_indicators = self.show_indicators) 
        
        self._reset_session()
        self.env_steps_size = n_iter
       

        # Random traversing => More unique data points:
        if not self.sequential:
            self.start_step = random.randint(self.lookback_window_size, self.df_total_steps - n_iter)
            self.end_step = self.start_step + n_iter
            
        # Sequential traversing:
        else: 
            self.start_step = self.lookback_window_size
            self.end_step = self.df_total_steps
            
        self.current_step = self.start_step
        
        # Refreshing memories => Filling last lookback_window_size # of observations:
        self._fill_history()
        
        debug = self.add_feature_shape() if self.debug else None

        state = np.concatenate([self.market_history, self.orders_history, self.indicators_history], axis=1)    
        state = self.apply_transform_obs(state) if self.transform_obs is not None else state 
        state = self.scaler.fit_transform(state) if self.normalize_obs else state

        return state
    
    def _fill_history(self):
        """ Refreshing memories => Filling last lookback_window_size # of observations """
        for i in reversed(range(self.lookback_window_size)):
            current_step = self.current_step - i
            self.orders_history.append(self.get_order_history())
            self.market_history.append(self.get_market_history(current_step))
            self.indicators_history.append(self.get_indicator_history(current_step))
        
    
    def add_feature_shape(self):
        """ Adds market, order and indicator history shape to shape dict for debugging """
        self.shapes['market_history'] = np.shape(self.market_history)
        self.shapes['orders_history'] = np.shape(self.orders_history)
        self.shapes['indicators_history'] = np.shape(self.indicators_history)
    
    def _next_observation(self):
        """ Get new the data points """
        self.market_history.append(self.get_market_history(self.current_step))
        self.indicators_history.append(self.get_indicator_history(self.current_step))

        obs = np.concatenate([self.market_history, self.orders_history, self.indicators_history], axis=1) 

        if self.transform_obs is not None:
            obs = self.apply_transform_obs(obs)
            
        if self.normalize_obs:
            obs = self.scaler.fit_transform(obs)

        return obs

    def _get_current_price(self):
        """ Returns current Open price """
        return self.df.loc[self.current_step, 'Open'] + (np.random.rand())
    
    def _get_random_current_price(self):
        """ Returns current price from Open and Close values randomly """
        return random.uniform(
            self.df.loc[self.current_step, 'Open'],
            self.df.loc[self.current_step, 'Close']
        ) + np.random.rand()

    def get_dcl(self):
        """ Returns dict of DCL """
        return {'Date'  : self.df.loc[self.current_step, 'Date'],
                'High'  : self.df.loc[self.current_step, 'High'],
                'Low'   : self.df.loc[self.current_step, 'Low']}
    
    def step(self, action):
        """ Performs one step (BUY, SELL, HOLD) with given action """
        self.crypto_bought = 0
        self.crypto_sold = 0
        self.current_step += 1

        action_type, amount = super(MultiContiniousStockEnvironment, self).action_decompose(action) #self.action_decompose(action)

        assert action_type in [0, 1, 2], f'Unknown action type found : {action_type}, should be in [0, 1, 2]'
        assert 0.0 <= amount <= 1.0, f'Unknown amount type found : {amount}, should be in [0, 0.1, 0.2, ... , 1]'

        current_price = self._get_random_current_price()
 
        self.adjust_slippage()
        
        

        if self.actions[action_type] == 'Hold':
            
            adjust_price = 1.0
            self.trades.append({**self.get_dcl(),
                    'total' : 0,
                    'type'  : "hold",
                    'current_price': current_price,
                    'adj_price': adjust_price})

        
        elif self.actions[action_type] == 'Buy' and self.balance > self.initial_balance * 1e-3:
            
            self.crypto_bought = self.balance / current_price * amount
            adjust_price = (1 + self.commission) * (1 + self.slippage) if self.adjust_price else 1.0
            self.balance -= (self.crypto_bought * current_price) * adjust_price
            self.crypto_held += self.crypto_bought

            self.trades.append({**self.get_dcl(),
                                'total' : self.crypto_bought,
                                'type'  : "buy",
                                'current_price': current_price,
                                'adj_price': adjust_price})

            self.episode_orders += 1
        
  
        elif self.actions[action_type] == 'Sell' and self.crypto_held > 0:
    
            self.crypto_sold = self.crypto_held * amount
            adjust_price = (1 - self.commission) * (1 - self.slippage) if self.adjust_price else 1.0
            self.balance += (self.crypto_sold * current_price)  * adjust_price
            self.crypto_held -= self.crypto_sold

            self.trades.append({**self.get_dcl(),
                                'total' : self.crypto_sold,
                                'type'  :  "sell",
                                'current_price': current_price,
                                'adj_price': adjust_price})
            self.episode_orders += 1

        self.net_worth = self.balance + self.crypto_held * current_price
        self.orders_history.append(self.get_order_history())
        
        reward = self.get_reward()
        done =  self.get_done()
        obs = self._next_observation()
        info = self.get_info() 
               
        
        self._update_net_worth()

        return obs, reward, done, info
    
    def _update_net_worth(self):
        """ Updates the previous net worth for rewarding actions and maximum net worth since episode"""
        self.prev_net_worth = self.net_worth
        
        if self.net_worth > self.max_net_worth:
            self.max_net_worth = self.net_worth
        
    
    def get_reward(self):
        """ Returns reward with initially given stragedy """
        
        if self.reward_strategy == 'incremental':
            reward = self.get_incremental_reward()
            
        elif self.reward_strategy == 'benchmark':
            reward = self.get_benchmark_reward()
            
        elif self.reward_strategy == 'sharp_ratio':
            reward = self.get_sharp_ratio_reward()
            
        elif self.reward_strategy == 'PnL':
            reward = self.get_PnL_reward()
            
        else:
            reward = self.get_base_reward()
            
        return reward
    
    def get_base_reward(self):
        """ Vanilla reward function, performs temporal difference between current and previous net worth"""
        return (self.net_worth - self.prev_net_worth) / self.prev_net_worth
    
    def get_PnL_reward(self):
        """ Returns PnL as percentage """
        return (self.net_worth - self.initial_balance) / self.initial_balance #* 100
    
    def get_done(self):
        """ Returns True if net worth is equal or smaller to %50 of initial balance or random traversing ends"""
        done = self.net_worth <= (self.initial_balance / 2) or self.current_step == self.end_step
        return done if isinstance(done, bool) else bool(done)
        
    
    def get_info(self):
        """ Returns a dict of last trade, orders, and shape of the matrices is debug True"""
        info =  {
                'trade'    :    self.trades[-1] if len(self.trades) > 0 else [],
                'order'    :    { 'balance'      : self.balance, 
                                  'net_worth'     : self.net_worth,
                                  'crypto_bought' : self.crypto_bought,
                                  'crypto_sold'   : self.crypto_sold,
                                  'crypto_held'   : self.crypto_held},
            
                'shapes'    :    self.shapes if self.debug else None,
                'Net Worth' :    self.net_worth,
                'Adj price' :    self.adjust_price
                }
        
        return self.render_excel()
    
    def render_excel(self):
        """ Returns a dict of last trade, orders, and shape of the matrices is debug True"""
        
            
        initial_trade = {'Date'  : self.df.loc[self.current_step, 'Date'],
                         'High'  : self.df.loc[self.current_step, 'High'],
                         'Low'   : self.df.loc[self.current_step, 'Low'],
                         'total' : 0.0,
                         'type'  :  "nothing",
                         'current_price': self._get_random_current_price(),
                         'adj_price' : 1.0}

        trades = self.trades[-1] if len(self.trades) > 0 else initial_trade
        
        
        return {
                **trades,
                **self.get_open_close(),
                'balance'       : self.balance, 
                'net_worth'     : self.net_worth,
                'crypto_bought' : self.crypto_bought,
                'crypto_sold'   : self.crypto_sold,
                'crypto_held'   : self.crypto_held,
                'profit'        : self.net_worth - self.initial_balance,
                'profit_percent(%)': (self.net_worth - self.initial_balance) / self.initial_balance * 100,
                **self._features,
                'Initial Balance'  : self.initial_balance,
                'Lookback_size'    : self.lookback_window_size,
                'Reward strategy'  : self.reward_strategy
                }
    
    
    def get_benchmark_reward(self):
        """ Returns reward as the squared distance between benchmark profit and current profit """
        profit_percent = (self.net_worth - self.initial_balance) / self.initial_balance * 100
        benchmark_profit = (self._get_current_price()  / self.df.loc[self.start_step, 'Open'] - 1) * 100
        diff = profit_percent - benchmark_profit
        reward = np.sign(diff) * (diff)**2
        return reward
       
    def get_incremental_reward(self):
        """ Returns a reward as a three way:
            1) Positive reward profits from holding asset while it is increasing
            2) Positive reward profits from not holding asset while it is decreasing 
            3) Negative reward for nothing (HOLD) for long time
        """
        self.punish_value += self.net_worth * 0.00001
        if self.episode_orders > 1 and self.episode_orders > self.prev_episode_orders:
            self.prev_episode_orders = self.episode_orders
            if self.trades[-1]['type'] == "buy" and self.trades[-2]['type'] == "sell":
                reward = self.trades[-2]['total']*self.trades[-2]['current_price'] - self.trades[-1]['total']*self.trades[-1]['current_price']
                reward -= self.punish_value
                self.punish_value = 0
                self.trades[-1]["Reward"] = reward
                return reward
            elif self.trades[-1]['type'] == "sell" and self.trades[-2]['type'] == "buy":
                reward = self.trades[-1]['total']*self.trades[-1]['current_price'] - self.trades[-2]['total']*self.trades[-2]['current_price']
                reward -= self.punish_value
                self.punish_value = 0
                self.trades[-1]["Reward"] = reward
                return reward
        else:
            return 0 - self.punish_value
        
    def get_sharp_ratio_reward(self):
        """ Returns sharp ratio as reward. The Sharpe ratio is a measure of risk-adjusted return. """
        # Use the percentage change method to easily calculate daily returns
        self.df['daily_ret'] = self.df['Close'].pct_change(1).fillna(method = 'backfill')

        # Assume an average minute wise risk-free rate over the period of 5%
        self.df['excess_daily_ret'] = self.df['daily_ret'] - self.excess_daily_return_ratio / self.trade_minutes_in_year

        # Return the annualised Sharpe ratio based on the excess daily returns
        return self.sharpe_ratio(self.df['excess_daily_ret'])
    
    def sharpe_ratio(self, daily_return):
      """Given a set of returns, calculates sharpe ratio """
      return (np.sqrt(self.trade_minutes_in_year) * np.mean(daily_return)) / np.std(daily_return)
        
    def seed(self, random_state = 42):
        """ Seed environment for reproducibility """
        self.np_random, random_state = seeding.np_random(random_state)
        return [random_state]
    
    def _seed_manuel(self, random_state = 42):
        """ Manuel random seed for reproducibility """
        np.random.seed(random_state)
        #torch.manual_seed(random_state)
        random.seed(random_state)
        return self
        

    def render(self, mode='human', close=False, visualize:bool = False,
               print_details:bool = False, render_to_json:bool = False,
               save_episode_excel:bool = False):
        
        """ Renders the environment by 3 different way: 
            1) Printing net worth and profit
            2) Writing the profit, balance, shares held, net worth to file
            3) Visualizations of live trading
        """

        if print_details:
            print('_' * 30)
            print(f'Step: {self.current_step}, \n Net Worth: {self.net_worth},\n Profit: {self.net_worth - self.initial_balance} \n')

        if render_to_json:
            json_print(self.render_excel())
            
        if save_episode_excel:
            raise NotImplementedError()

        if visualize:
            img = self.visualization.render(self.df.loc[self.current_step], self.net_worth, self.trades)
            return img
        
    def close(self):
        """ Closes the testing session """
        if self.sequential and self.visualize: 
            if self.visualization is not None:
                self.visualization.close()