import numpy as np
import pandas as pd


def drop_correlated_features(df:pd.DataFrame, threshold:float = 0.99):
    """ Given the data frame with numeric values, automatically drops the correlated columns. """
    corr_matrix = df.corr().abs()
    # Select upper triangle of correlation matrix
    upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
    to_drop = [column for column in upper.columns if any(upper[column] > threshold)]
    df.drop(to_drop, axis=1, inplace=True)
    return df