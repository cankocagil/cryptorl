
import pandas as pd
from ta.trend import SMAIndicator, macd, PSARIndicator
from ta.volatility import BollingerBands
from ta.momentum import rsi

import pandas as pd
from collections import deque
import matplotlib.pyplot as plt
from mplfinance.original_flavor import candlestick_ohlc
import matplotlib.dates as mpl_dates
from matplotlib import ticker
from datetime import datetime
import os
import numpy as np

import pandas_ta


def add_indicator(df:pd.DataFrame, 
                  indicator_list:list = ['sma7','sma25','sma99','bb_bbm','bb_bbh',
                                         'bb_bbl','psar','MACD','RSI','ema7','ema25',
                                         'ema99', 'super_trend', 'super_trend_s',
                                         'super_trend_l', 'log_return']
    ):
    
    """ Adding list of indicators and fill nan-values """
    
    assert all(indicator in ['sma7','sma25','sma99','bb_bbm','bb_bbh',
                             'bb_bbl','psar','MACD','RSI','ema7','ema25',
                             'ema99', 'super_trend', 'super_trend_s',
                             'super_trend_l', 'log_return'] 
           for indicator in indicator_list), f'Unknown indicator {indicator}!'
    
    if 'sma7' in indicator_list:
        df["sma7"] = SMAIndicator(close=df["Close"], window=7, fillna=True).sma_indicator()
     
    if 'sma25' in indicator_list:
        df["sma25"] = SMAIndicator(close=df["Close"], window=25, fillna=True).sma_indicator()
        
    if 'sma99' in indicator_list:
        df["sma99"] = SMAIndicator(close=df["Close"], window=99, fillna=True).sma_indicator()
        
    if 'ema7' in indicator_list:   
        df["ema7"] = pandas_ta.ema(df["Close"], length=7).fillna(method = 'backfill')
        
    if 'ema25' in indicator_list:   
        df["ema25"] = pandas_ta.ema(df["Close"], length=25).fillna(method = 'backfill')

    if 'ema99' in indicator_list:   
        df["ema99"] = pandas_ta.ema(df["Close"], length=99).fillna(method = 'backfill')
    
    indicator_bb = BollingerBands(close=df["Close"], window=20, window_dev=2)
    
    if 'bb_bbm' in indicator_list:
        df['bb_bbm'] = indicator_bb.bollinger_mavg().fillna(method = 'backfill')
        
    if 'bb_bbh' in indicator_list:     
        df['bb_bbh'] = indicator_bb.bollinger_hband().fillna(method = 'backfill')
        
    if 'bb_bbl' in indicator_list: 
        df['bb_bbl'] = indicator_bb.bollinger_lband().fillna(method = 'backfill')

    if 'psar' in indicator_list: 
        indicator_psar = PSARIndicator(high=df["High"], low=df["Low"], close=df["Close"], step=0.02, max_step=2, fillna=True)
        df['psar'] = indicator_psar.psar()
    
    if 'MACD' in indicator_list: 
        df["MACD"] = macd(close=df["Close"], window_slow=26, window_fast=12, fillna=True) 
    
    if 'RSI' in indicator_list: 
        df["RSI"] = rsi(close=df["Close"], window=14, fillna=True)
        
        
    if 'super_trend' in indicator_list: 
        super_trend = pandas_ta.supertrend(high=df["High"], low = df["Low"], close = df["Close"], length = 10, multiplier=4.0)
        df['super_trend'] = super_trend['SUPERT_10_4.0'].fillna(method = 'backfill')
        
        if 'super_trend_s' in indicator_list: 
            df['super_trend_s'] = super_trend['SUPERTl_10_4.0'].fillna(method = 'backfill')

        if 'super_trend_l' in indicator_list: 
            df['super_trend_l'] = super_trend['SUPERT_10_4.0'].fillna(method = 'backfill')
        
    if 'log_return' in indicator_list: 
        df['log_return'] = pandas_ta.log_return(df['Close'], cumulative=False).fillna(method = 'backfill')
        
    if 'percent_return' in indicator_list: 
        df['percent_return'] = pandas_ta.percent_return(df['Close'], cumulative=False).fillna(method = 'backfill')   
    
    return df

def AddIndicators(df):
    # Add Simple Moving Average (SMA) indicators
    df["sma7"] = SMAIndicator(close=df["Close"], window=7, fillna=True).sma_indicator()
    df["sma25"] = SMAIndicator(close=df["Close"], window=25, fillna=True).sma_indicator()
    df["sma99"] = SMAIndicator(close=df["Close"], window=99, fillna=True).sma_indicator()
    
    # Add Bollinger Bands indicator
    indicator_bb = BollingerBands(close=df["Close"], window=20, window_dev=2)
    df['bb_bbm'] = indicator_bb.bollinger_mavg()
    df['bb_bbh'] = indicator_bb.bollinger_hband()
    df['bb_bbl'] = indicator_bb.bollinger_lband()

    # Add Parabolic Stop and Reverse (Parabolic SAR) indicator
    indicator_psar = PSARIndicator(high=df["High"], low=df["Low"], close=df["Close"], step=0.02, max_step=2, fillna=True)
    df['psar'] = indicator_psar.psar()

    # Add Moving Average Convergence Divergence (MACD) indicator
    df["MACD"] = macd(close=df["Close"], window_slow=26, window_fast=12, fillna=True) # mazas

    # Add Relative Strength Index (RSI) indicator
    df["RSI"] = rsi(close=df["Close"], window=14, fillna=True) # mazas
    
    return df

def Plot_OHCL(df):
    df_original = df.copy()
    # necessary convert to datetime
    df["Date"] = pd.to_datetime(df.Date)
    df["Date"] = df["Date"].apply(mpl_dates.date2num)

    df = df[['Date', 'Open', 'High', 'Low', 'Close', 'Volume']]
    
    # We are using the style ‘ggplot’
    plt.style.use('ggplot')
    
    # figsize attribute allows us to specify the width and height of a figure in unit inches
    fig = plt.figure(figsize=(16,8)) 

    # Create top subplot for price axis
    ax1 = plt.subplot2grid((6,1), (0,0), rowspan=5, colspan=1)

    # Create bottom subplot for volume which shares its x-axis
    ax2 = plt.subplot2grid((6,1), (5,0), rowspan=1, colspan=1, sharex=ax1)

    candlestick_ohlc(ax1, df.values, width=0.8/24, colorup='green', colordown='red', alpha=0.8)
    ax1.set_ylabel('Price', fontsize=12)
    plt.xlabel('Date')
    plt.xticks(rotation=45)

    # Add Simple Moving Average
    ax1.plot(df["Date"], df_original['sma7'],'-', legend = 'SMA7')
    ax1.plot(df["Date"], df_original['sma25'],'-', legend = 'SMA25')
    ax1.plot(df["Date"], df_original['sma99'],'-', legend = 'SMA99')

    # Add Bollinger Bands
    ax1.plot(df["Date"], df_original['bb_bbm'],'-', legend = 'BB_BBM')
    ax1.plot(df["Date"], df_original['bb_bbh'],'-', legend = 'BB_BBH')
    ax1.plot(df["Date"], df_original['bb_bbl'],'-', legend = 'BB_BBL')

    # Add Parabolic Stop and Reverse
    ax1.plot(df["Date"], df_original['psar'],'.', legend = 'PSAR')



    # # Add Moving Average Convergence Divergence
    ax2.plot(df["Date"], df_original['MACD'],'-', legend = 'MovAveConvDiv')

    # # Add Relative Strength Index
    ax2.plot(df["Date"], df_original['RSI'],'-', legend = 'RSI')

    ax1.legend()
    ax2.legend()

    
    # beautify the x-labels (Our Date format)
    ax1.xaxis.set_major_formatter(mpl_dates.DateFormatter('%y-%m-%d'))# %H:%M:%S'))
    fig.autofmt_xdate()
    fig.tight_layout()

    #plt.draw()
    plt.show()


if __name__ == "__main__":   
    df = pd.read_csv('./data/pricedata.csv')
    df = df.sort_values('Date')
    df = AddIndicators(df)

    test_df = df[-400:]

    Plot_OHCL(df)
