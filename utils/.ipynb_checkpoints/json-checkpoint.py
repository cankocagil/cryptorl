import json

def json_print(parsed):
    print(json.dumps(parsed, indent=4, sort_keys=True))
