from datetime import datetime
import os 
import pandas as pd
import numpy as np
from preprocessing.indicators import AddIndicators, add_indicator

def preprocess(df_path:str = "" , indicator_list:list = [], save_preprocessed:bool = True):
    """ Given the path of the data frame consist of OHLCV with date times, reads and adds indicator
        with given list. Save preprocessed data frame if save_preprocessed True """
    
    df = pd.read_csv(df_path)
    df = df[['Date','Open','High','Low','Close', 'Volume']].sort_values('Date')
    
    df['Date'] = df['Date'].apply(lambda x : x[:16])

    assert unique_cols(df['Date'].apply(lambda x : len(x))), 'Date length must be same!'
    
    if len(indicator_list) > 0:
        
        ind = "".join([indicator[0] for indicator in indicator_list])
        
        df_ind_path = f'{df_path}_with_indicators_{ind}.csv'
        
        if not os.path.exists(df_ind_path):
            df = add_indicator(df, indicator_list)
            
            if save_preprocessed:
                print(f' Preprocessed data frame is saved to the location: {df_ind_path}')
                df.to_csv(df_ind_path)
            
        else:
            print(f' Preprocessed data fetched from the location: {df_ind_path}')
            df = pd.read_csv(df_ind_path)
            
    if 'Unnamed: 0' in df.columns.tolist():
        df.drop(['Unnamed: 0'], 1, inplace = True)
        
    return df
        
def unique_cols(df):
    a = df.to_numpy()
    return (a[0] == a).all(0)

def preprocess_price(df, add_indicators:bool = True):
    return AddIndicators(df)

def preprocess_df(df, add_indicators:bool = True):

    df = df[['Date','Open','High','Low','Close', 'Volume']]
    df = df.sort_values('Date')
    df['Date'] = df['Date'].apply(lambda x : x[:16])

    assert unique_cols(df['Date'].apply(lambda x : len(x))), 'Date length must be unique'

    if add_indicators:
        print('Adding indicators...')
        if not os.path.exists('./data/BTCUSDT-1m-data_historical_indicators.csv'):
            df = AddIndicators(df)
            df.to_csv('./data/BTCUSDT-1m-data_historical_indicators.csv')
        else:
            print('Data is fetched from path!')
            df = pd.read_csv('./data/BTCUSDT-1m-data_historical_indicators.csv')
    return df




def unique_cols(df):
    a = df.to_numpy()
    return (a[0] == a).all(0)


def write_to_file(Date, net_worth, filename='{}.txt'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))):
    for i in net_worth: 
        Date += " {}".format(i)
    #print(Date)
    if not os.path.exists('logs'):
        os.makedirs('logs')
    file = open("logs/"+filename, 'a+')
    file.write(Date+"\n")
    file.close()
