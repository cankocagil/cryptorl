import numpy as np


class RandomWalk(object):
    def __init__(self):
        pass
    def walk(self, env, num_episodes, batch_size):
        return random_walk(env, num_episodes, batch_size)
    

def random_walk(env, num_episodes, batch_size):
    all_episode_rewards = []
       
    for episode in range(num_episodes):
        state = env.reset()
        episode_rewards = []
        for t in range(batch_size):
            env.render()
            action = env.action_space.sample()
            next_state, reward, done, info = env.step(action)
            state = next_state

            if env.current_step == env.end_step:
                state = env.reset()

            episode_rewards.append(reward)

        all_episode_rewards.append(sum(episode_rewards))

    mean_episode_reward = np.mean(all_episode_rewards)
    std_episode_reward = np.std(all_episode_rewards)
    
    print("Mean reward:", mean_episode_reward, 'std episode reward', std_episode_reward, "Num episodes:", num_episodes)
    
    return mean_episode_reward, std_episode_reward
    